#include "ConstructSVG.h"

void ConstructSVG::parseToken(std::string& token)
{
    int first = token.find_first_of("\"");
    int last = token.find_last_of("\"");
    token = token.substr(first + 1, last - first - 1);
}

bool ConstructSVG::pullCounties(std::string filePath)
{
  while(counties.size() > 0)
  {
    counties.pop_back();
  }

  std::ifstream inFile(filePath + ".paths");
  if(inFile.fail())
  {
    std::cout << "[Error] Could not open input file." << std::endl;
    return false; 
  }
  
  std::string line;
  while(std::getline(inFile, line))
  {
    std::stringstream ss;
    County tempCounty;
    ss << line;
    //split the line into the token values
    std::string token;
    ss >> token;
    parseToken(token);
    tempCounty.id = token;
    ss >> token;
    parseToken(token);
    tempCounty._class = token;
    std::getline(ss,token);
    parseToken(token);
    tempCounty.d = token;
    counties.push_back(tempCounty); 
  }
  return true;
}

bool ConstructSVG::readData(ConstructSVG& csvg, std::string fileLoc, int actionIndex)
{
  std::ifstream data(fileLoc + "/" + actions[actionIndex].dataFile);
  if(data.fail())
  {
    std::cout << "[Error] Action(" << actionIndex << ") data file {" << fileLoc + "/" + actions[actionIndex].dataFile << "} could not be found/opened!\n";
    return false;
  }

  std::string line;
  int MAX = 0, rank;
  while(std::getline(data,line))
  {
    std::stringstream ss(line);
    std::getline(ss,line,',');//throw away content that isn't needed
    std::getline(ss,line,',');//throw away content that isn't needed
    ss >> rank;
    if(rank > MAX)
      MAX = rank;
  }
  csvg.MAX = MAX;
  csvg.r1 = MAX/4;
  csvg.r2 = csvg.r1 * 2;
  csvg.r3 = csvg.r1 * 3;
  return true;
}

bool ConstructSVG::applyData(ConstructSVG& csvg, std::string fileLoc, int actionIndex)
{
  std::ifstream data(fileLoc + "/" + actions[actionIndex].dataFile);
  if(data.fail())
  {
    std::cout << "[Error] Action(" << actionIndex << ") data file {" << fileLoc + "/" + actions[actionIndex].dataFile << "} could not be found/opened!\n";
    return false;
  }

  std::string county,line;
  int rank;
  while(std::getline(data,line))
  {
    std::stringstream ss(line);
    std::getline(ss,county,',');
    std::getline(ss,line,',');//throw away content that isn't needed
    ss >> rank;
    //search for correct county
    for(int i = 0; i < counties.size(); ++i)
    {
      if(counties[i].id == county)
      {
        //give that county a rank/tier color
        if(rank > 0 && rank <= csvg.r1)
        {
          counties[i]._class += " tier1";
        }else if(csvg.r1 > 0 && rank <= csvg.r2)
        {
          counties[i]._class += " tier2";
        }else if(csvg.r2 > 0 && rank <= csvg.r3)
        {
          counties[i]._class += " tier3";
        }else if(csvg.r3 > 0 && rank <= csvg.MAX)
        {
          counties[i]._class += " tier4";
        }
      }
    }
  }

  return true;
}
bool ConstructSVG::genSVG(ConstructSVG& csvg, std::string filePath, int actionIndex)
{
  std::string header = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
  "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:amcharts=\"http://amcharts.com/ammap\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"750\" height=\"830\">\n"
	"<defs>\n"
	"	<style type=\"text/css\">\n"
	"		.county\n"
	"		{\n"
	"			fill: #CCCCCC;\n"
	"			fill-opacity: 1;\n"
	"			stroke:white;\n"
	"			stroke-opacity: 1;\n"
	"			stroke-width:0.5;\n"
	"		}\n"
	"		.stroke{\n"
	"		stroke-width: 1;\n"
	"		stroke: rgb(0,0,0);\n"
	"		}\n"
	"		.small { \n"
	"			font: bold 25px sans-serif; \n"
	"			margin: auto;\n"
	"			}\n"
	"			.18pt{\n"
	"			font: bold 18px sans-serif;\n"
	"			margin: auto;\n"
	"			}\n"
  "			.pt12{\n"
	"			font: bold 12px sans-sarif;\n"
	"			}\n"
	"		.tier1\n"
	"		{\n"
	"			fill: " + actions[actionIndex].t1 +";\n"
	"			fill-opacity: .8;\n"
	"		}\n"
	"		.tier2\n"
	"		{\n"
	"			fill: " + actions[actionIndex].t2 +";\n"
	"			fill-opacity: .8;\n"
	"		}\n"
	"		.tier3\n"
	"		{\n"
	"			fill: " + actions[actionIndex].t3 +";\n"
	"			fill-opacity: .8;\n"
	"		}\n"
	"		.tier4\n"
	"		{\n"
	"			fill: " + actions[actionIndex].t4 +";\n"
	"			fill-opacity: .8;\n"
	"		}\n"
	"	</style>\n"
  "</defs> \n"
  "<g id=\"counties\" transform=\"translate(100,100) scale(1.5)\">\n";
  

  std::string footer = 
  "\n</g>\n"
	"<g>\n"
	"  <text class=\"small\" transform=\"translate(100, 50)\">" + actions[actionIndex].label + " - Wisconsin</text>\n"
	"</g>\n"
	"<g>\n"
	"  <text style=\"font:bold 18px sans-\" transform=\"translate(35, 800)\">" + actions[actionIndex].label + " ranks</text>\n"
	"  <rect class=\"tier1 stroke\" width=\"30\" height=\"30\" transform=\"translate(300,780)\"/>\n"
	"  <text class=\"pt12\" transform=\"translate(340, 800)\">1 to " + std::to_string(csvg.r1) + "</text>\n"
	"  <rect class=\"tier2 stroke\" width=\"30\" height=\"30\" transform=\"translate(400,780)\"/>\n"
	"  <text class=\"pt12\" transform=\"translate(440, 800)\">" + std::to_string(csvg.r1 + 1) + " to " + std::to_string(csvg.r2) + "</text>\n"
	"  <rect class=\"tier3 stroke\" width=\"30\" height=\"30\" transform=\"translate(505,780)\"/>\n"
	"  <text class=\"pt12\" transform=\"translate(540, 800)\">" + std::to_string(csvg.r2 + 1) + " to " + std::to_string(csvg.r3) + "</text>\n"
	"  <rect class=\"tier4 stroke\" width=\"30\" height=\"30\" transform=\"translate(605,780)\"/>\n"
	"  <text class=\"pt12\" transform=\"translate(640, 800)\">" + std::to_string(csvg.r3 + 1) + " to " + std::to_string(csvg.MAX) + "</text>\n"
	"</g>\n"
"</svg>\n";

  std::ofstream outFile(filePath + "/" + actions[actionIndex].dataFile + "_Output.svg");
  if(outFile.fail())
  {
    std::cout << "[Error] Failed to create and/or open output file {" << filePath + "_Output.svg" << "}.\n";
    return false;
  }

  outFile << header;
  for(int i = 0; i < counties.size(); ++i)
  {
    outFile << "  <path id=\"" << counties[i].id << "\" class=\"" << counties[i]._class << "\" d=\"" << counties[i].d << "\"/>\n";
  }
  outFile << footer;

  return true;
}
bool ConstructSVG::convertSVGtoPNG()
{
  return false;
}

//-------------------------------------------------

int main(void)
{
  std::string filePaths, fileLogs, line;
  ConstructSVG csvg;
  std::cout << "[Log] Enter the folder location of the data: ";
  std::cin >> filePaths;
  //------------------------------------------------------------
  std::cout << "[Log] Reading in actions.";
  std::ifstream actionFile("actions.txt");
  if(actionFile.fail())
  {
    std::cout << "[Error] Could not find/open actions.txt!\n";
  }

  while(std::getline(actionFile, line))
  {
    Actions tempAction;
    tempAction.label = line;
    std::getline(actionFile,line);
    std::stringstream ss(line);
    ss >> tempAction.t1;
    ss >> tempAction.t2;
    ss >> tempAction.t3;
    ss >> tempAction.t4;
    std::getline(actionFile,tempAction.dataFile);
    csvg.insertAction(tempAction);
  }

  std::cout << "[Progress] Starting Map Generation for " << csvg.getNumMaps() << " maps.\n";
  if(csvg.pullCounties("Wisconsin_Counties"))
  {
    std::cout << "[Log] Pulled SVG information from Wisconsin_Counties.paths.\n";
  }
  for(int i = 0; i < csvg.getNumMaps(); ++i)
  {
    std::cout << "[Progress] Calculating data for map " << i + 1 << ".\n";
    if(!csvg.readData(csvg,filePaths, i))
    {
      break;
    }
    std::cout << "[Progress] Appling calculated data for map " << i + 1 << ".\n";
    if(!csvg.applyData(csvg, filePaths, i))
    {
      break;
    }
    //Generate the map svg
    std::cout << "[Progress] Constructing SVG for map " << i + 1 << ".\n";
    if(!csvg.genSVG(csvg,filePaths, i))
    {
      break;
    }
    std::cout << "[Log] Generated SVG File at {" << filePaths << "/" << csvg.getDataFile(i) + "_Output.svg" << "}\n";
  }
  return 0;
}
