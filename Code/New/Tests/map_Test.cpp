#include "../Structs.h"
#include <iostream>

void printMap(map);

int main() {

  map map1;
  std::cout << map1.print();
  std::cout << "Adding title..." << std::endl;
  map1.setTitle("Population by State - ");
  std::cout << map1.print();
  std::cout << "Adding location..." << std::endl;
  map1.setLocation("USA");
  std::cout << map1.print();
  std::cout << "Adding Colors..." << std::endl;
  map1.addColor("#cccccc");
  std::cout << map1.print();
  map1.addColor("#3f3f3f");
  std::cout << map1.print();

  return 0;
}
